# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=mold
pkgver=1.1
pkgrel=1
pkgdesc="fast modern linker"
url="https://github.com/rui314/mold"
arch="x86_64" # only x86_64 and x86 supported, x86 bundled tbb build fail
license="AGPL-3.0" # not specified if -only
makedepends="
	clang
	cmake
	libtbb-dev
	linux-headers
	llvm-dev
	mimalloc2-dev
	openssl-dev
	zlib-dev
	"
checkdepends="
	bash
	dwarf-tools
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rui314/mold/archive/refs/tags/v$pkgver.tar.gz"

build() {
	export SYSTEM_MIMALLOC=1
	export SYSTEM_TBB=1
	export LTO=1

	make CC=clang \
		CXX=clang++ \
		PREFIX=/usr \
		CFLAGS="$CFLAGS -O2" \
		CXXFLAGS="$CXXFLAGS -O2"
}

check() {
	rm test/elf/shuffle-sections.sh # fails randomly
	make check
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install
}

sha512sums="
b8b125c77563abe2741c2f32d73ab2b12dff275b92b633fcd015336c6ea735e38e95c3d2afb50e5baace2023d4f9de3eb50d53053eabc2cd2f045110eef4cc1e  mold-1.1.tar.gz
"
