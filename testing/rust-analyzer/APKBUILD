# Contributor: S.M Mukarram Nainar <theone@sm2n.ca>
# Maintainer: S.M Mukarram Nainar <theone@sm2n.ca>
pkgname=rust-analyzer
pkgver=2022.02.21
_pkgver=${pkgver//./-}
pkgrel=0
pkgdesc="A Rust compiler front-end for IDEs"
url="https://github.com/rust-analyzer/rust-analyzer"
# limited by rust, x86/armhf/armv7 fail tests
arch="x86_64 aarch64 ppc64le"
license="MIT OR Apache-2.0"
depends="rust-src"
makedepends="cargo"
checkdepends="rustfmt"
subpackages="$pkgname-doc"
source="https://github.com/rust-analyzer/rust-analyzer/archive/$_pkgver/rust-analyzer-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release --manifest-path crates/rust-analyzer/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin target/release/rust-analyzer

	install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE-MIT LICENSE-APACHE
	install -Dm644 docs/user/manual.adoc "$pkgdir"/usr/share/doc/$pkgname/manual.adoc
}

sha512sums="
16a17151d211a42beb88066d405875c4655ce9b50f79033c029684c77b7e5c5088625ee7565322b7ffe79905d2c86e180a0434261d2f07e9fdaeebe377663690  rust-analyzer-2022.02.21.tar.gz
"
