# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=cantor
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
# ppc64le blocked by qt5-qtwebengine
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://edu.kde.org/cantor/"
pkgdesc="KDE Frontend to Mathematical Software "
license="GPL-2.0-or-later"
makedepends="
	analitza-dev
	discount-dev
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kparts-dev
	kpty-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	poppler-qt5-dev
	python3-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtwebengine-dev
	qt5-qtxmlpatterns-dev
	syntax-highlighting-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/cantor-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7dda501d966b999d06d42c8b20ea2053bef04c6deac53b3c625b1f78ad5b79a1c603f5207a2207421db51c90f91958b31e464ef7e7c59930e5d8a6b1263d0689  cantor-21.12.2.tar.xz
"
