# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkcompactdisc
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> solid
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="Library for interfacing with CDs"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	phonon-dev
	qt5-qtbase-dev
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkcompactdisc-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7baeb0dabd362109240a0d818897820d43c191b581f6d397458bb1b698da3a5de156df77142b50f040614ed8ea29bec127dc98b2f3e667d90cbb0f985efcf052  libkcompactdisc-21.12.2.tar.xz
"
