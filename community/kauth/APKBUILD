# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kauth
pkgver=5.91.0
pkgrel=0
pkgdesc="Abstraction to system policy and authentication features"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcoreaddons-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kauth-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# KAuthHelperTest hangs
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(KAuthHelperTest)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
eca8dab2c49240736bb4bad97799616305dd1f679528e05634ad8c9344be80dd584ed5c6056f55348575b5c5565bedef627836ac315aaad7bf2d59686340955d  kauth-5.91.0.tar.xz
"
