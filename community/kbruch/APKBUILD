# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kbruch
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/kbruch/"
pkgdesc="Practice Fractions"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbruch-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4bf87e301215ca9108d64d898f990fee000ca47961fb9f77e09d900314eb9c2d09506aa0c459933330690fb9ed8d9ff95a7194b287957f2be8c4dcfc96398044  kbruch-21.12.2.tar.xz
"
