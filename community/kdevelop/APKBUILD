# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdevelop
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://www.kdevelop.org/"
pkgdesc="A featureful, plugin-extensible IDE for C/C++ and other programming languages"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
depends="indent"
makedepends="
	boost-dev
	clang>=12
	clang-dev>=12
	clang-libs>=12
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	kguiaddons-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kjobwidgets-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kservice-dev
	ktexteditor-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkomparediff2-dev
	libksysguard-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	shared-mime-info
	threadweaver-dev
	"
makedepends="$makedepends
	llvm-dev>=12
	" # Should always install the version of llvm that contains /usr/bin/llvm-config
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdevelop-$pkgver.tar.xz
	fix-find-clang-path.patch
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a7e52593ba1297e60058329e38b45b3b3abc235b7f009a4b7e137efc18bc93f79ce146b7c883aed4e4fc7960d07c096013cf4b8265e5b2dfea5c286f1d8c6796  kdevelop-21.12.2.tar.xz
6700fcf1da4243aa9950c8c81e47f916d3d04cd11a73fbf14bbe77209fd19000078f49c266d0b58158d7f85cdd89c0316d86284bf665470a5cfb2c8318bdf56d  fix-find-clang-path.patch
"
