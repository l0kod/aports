# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=grantlee-editor
pkgver=21.12.2
pkgrel=0
pkgdesc="Utilities and tools to manage themes in KDE PIM applications "
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-mime-dev
	extra-cmake-modules
	grantleetheme-dev
	karchive-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kimap-dev
	knewstuff-dev
	kpimtextedit-dev
	ktexteditor-dev
	kxmlgui-dev
	libkleo-dev
	messagelib-dev
	pimcommon-dev
	qgpgme
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	syntax-highlighting-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantlee-editor-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
54f1f108eff1c649918fe11d0ab020ab05206536f940ca930a6699b8e6ef2dc323065398ff9ba87d938fd5bf58db18d7d409cb686ffda2f7cf621af6c578618c  grantlee-editor-21.12.2.tar.xz
"
