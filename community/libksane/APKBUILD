# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libksane
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by ktextwidgest, kwallet
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/graphics/"
pkgdesc="An image scanning library"
license="LGPL-2.1-only OR LGPL-3.0-only"
makedepends="
	extra-cmake-modules
	ki18n-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	sane-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksane-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ecfa0499898ea7d1d8721040093fe83e620cb283653f564297edc6e59aecc4fffd5d15c577bca05661ba29772eabb300510a9ed14d1d97ff9799248b06fbe415  libksane-21.12.2.tar.xz
"
