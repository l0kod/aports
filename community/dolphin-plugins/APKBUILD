# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dolphin-plugins
pkgver=21.12.2
pkgrel=0
url="https://www.kde.org/applications/system/dolphin/"
pkgdesc="Extra Dolphin plugins"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
license="GPL-2.0-or-later"
makedepends="
	dolphin-dev
	extra-cmake-modules
	ki18n-dev
	kio-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-plugins-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_svn=OFF # Broken "error: expected unqualified-id before '(' token"
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
44e93558802282f3a73aaac9443dcded8498499c4ab400dedb68572af4add007dcbae6424a99929da6c5ab03c7c125beee313fe4b14b5ee29147938418410dfc  dolphin-plugins-21.12.2.tar.xz
"
