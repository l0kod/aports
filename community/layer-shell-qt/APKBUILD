# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=layer-shell-qt
pkgver=5.24.2
pkgrel=0
pkgdesc="Qt component to allow applications to make use of the Wayland wl-layer-shell protocol"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	qt5-qtdeclarative-dev
	qt5-qtwayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/layer-shell-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DENABLE_TESTING=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
43671952003282b84a3ac8aab736d2d52fb9d84caed790484b14347ef8ce6691478397cf268b7bd2019afbd6dee7a399ba902e42c20f483d9328fb9dd3935c5e  layer-shell-qt-5.24.2.tar.xz
"
