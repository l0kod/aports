# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcompletion
pkgver=5.91.0
pkgrel=0
pkgdesc="Text completion helpers and widgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kwidgetsaddons-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcompletion-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c21dbf1507369551eab87fa2e5e160937f3488fd00bf26dfb6c8856456ad04271bbb8bc84f20d6fb07ce9f2325b7b4df44ee1036a443c3d32e27f8968ab7714f  kcompletion-5.91.0.tar.xz
"
