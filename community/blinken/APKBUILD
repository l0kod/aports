# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=blinken
pkgver=21.12.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/blinken/"
pkgdesc="Memory Enhancement Game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/blinken-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a6443ba27023cf3e88903690357be410c79af5585c314c609b69c94a2db7ae4104809fa7e518a80f470753c80c6b439e40ef0ae4e89df0a2257ed7f02cf8b198  blinken-21.12.2.tar.xz
"
